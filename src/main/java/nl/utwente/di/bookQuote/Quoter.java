package nl.utwente.di.bookQuote;

import javax.swing.plaf.PanelUI;
import java.util.HashMap;
import java.util.Map;
public class Quoter {
    private Map<String, Double> bookPrices = new HashMap<>();

    public Quoter() {
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
    }

    public double getBookPrice(String isbn){
        return bookPrices.getOrDefault(isbn, 0.0);
    }
}
